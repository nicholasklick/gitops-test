# What is this project about

We have 2 related projects to show you a recommended setup for GitLab's GitLab Kubernetes Agent. This repository contains the application logic, and the templates to deploy the application to Kubernetes. The - so called - [manifest repository](https://gitlab.com/nagyv-gitlab/gitops-test-manifest/) contains the Kubernetes manifest files that are actually being deployed to Kubernetes.

Whenever this repository changes, a new docker image is generated, and the manifest repo is updated to trigger a new deployment.

## Various deployment setups

Current setup:

- A commit in the app repo's branch X, updates the kubernetes manifest files in the manifest project under `/environments/<branch>`. This way master, staging and development can be their own clusters.

Next to do:

- A commit in the app repo's master and staging branches, updates the kubernetes manifest files in the manifest project under `/environments/<branch>`. Commits in other branches do nothing. A `~review-app` label on an MR updates the `/environments/dev` directory manifest files with new manifests files to deploy a review app in the namespace `review-<mr id>`.
- Move the helm template over in the manifest project.

## Docs

- https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent
